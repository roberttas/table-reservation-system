<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = [
		'name',
        'table_id',
        'contact_phone',
        'reserved_at',
        'number_of_persons'
        

    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }



      public function orderItems(){
    	return $this->hasMany('App\OrderItem');
    }

    public function table(){
    	return $this->belongsTo('App\Table');
    }
}
