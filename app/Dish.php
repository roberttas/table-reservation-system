<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $fillable = [
        'title',
        'photo',
        'description',
        'netto_price',
        'price',
        'quantity',
        'menu_id'
    ];
                          

    public function menu(){
    	return $this->belongsTo('App\Menu'); 
    }
    
	public function carts(){
		return $this->belongsToMany('Add\Cart');
	}


    public function orderItem(){
    	// Has Many Through sarysis
    	return $this->hasManyThrough('App\Order', 'App\OrderItem');
    }

}
