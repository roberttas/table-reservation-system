<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
	protected $fillable = [
        'title',
        'description',
        'netto_price',
        'price',
        'quantity',
        'order_id',
        'dish_id'

    ];

    public function order(){
    	return $this->belongsTo('App\Order');
    }

    public function dish(){
    	return $this->hasMany('App\Dish');
    }
}
