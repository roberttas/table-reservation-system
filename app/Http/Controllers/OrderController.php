<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $user = Auth::user();

        // ar prisijunges ar ne

        // jei neprisijunges
        if (Auth::guest()) {
        return redirect()->route('register');
        }
        //jeigu prisijunges 

        return view('order.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validacija
        $this->validate($request, [
        'name' => 'required',
        'table_id' => 'required',
        'contact_phone' => 'required',
        'reserved_at' => 'required|date_format:Y-m-d H:i',
        'number_of_persons' => 'required'
        ]);

        //surinkt duomenis is formos
        $data = $request->all();
        //issaugoti orderi duomenu bazeje
        $user = Auth::user();
        $order = $user->orders()->create($data);

        $cartItems = session()->get('cart.items');

        foreach ($cartItems as $item) {
            $item['dish_id'] = $item['id'];
            $order->orderItems()->create($item);
        }

        $request->session()->forget('cart.items');
        $request->session()->put('cart.total_amount', 0);
        $request->session()->put('cart.total_items', 0);
        //peradresuoja vartotoja i uzsakymo psl
        return redirect()->route('order.show', $order->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        

        return view('order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
