<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
public function store(Request $request)
    {        
        
        $total_items = 0;
        $total_amount = 0;
        $items = [];

        // Surandam pridedamą patiekalą DB
        $id = $request->dish_id;
        $dish = \App\Dish::find($id)->toArray();
        $dish['cart_count'] = 1;

        $items = $request->session()->get('cart.items');

        if($items) {

            $found = false;
            for($i = 0; $i < count($items); $i++){
                if($items[$i]['id'] == $dish['id']){
                    $items[$i]['cart_count']++;
                    $found = true;
                    break;
                }
            }

            if(!$found){
                $items[] = $dish;
            }

            for($i = 0; $i < count($items); $i++){
                $total_amount += $items[$i]['cart_count'] * $items[$i]['price'];
                $total_items += $items[$i]['cart_count'];
            }

        } else {
            $items[] = $dish;
            $total_amount = $dish['price'];
            $total_items = 1;
        }

        $request->session()->put('cart.items', $items);
        $request->session()->put('cart.total_items', $total_items);
        $request->session()->put('cart.total_amount', $total_amount);
       
        // Atiduodam rezultatą
        $result = [
            "total_items"   => $total_items,
            "total_amount"  => $total_amount,
            "items"         => $items
        ];

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */  

    public function destroyAll(Request $request)
    {
        $request->session()->forget('cart.items');
        $request->session()->put('cart.total_items', 0);
        $request->session()->put('cart.total_amount', 0);
        return;
    }

    public function destroy(Request $request)
    {        
        
        $total_items = 0;
        $total_amount = 0;
        $items = [];

        // Surandam pridedamą patiekalą DB
        $id = $request->dish_id;

        $items = $request->session()->get('cart.items');

        for($i = 0; $i < count($items); $i++){
            if($items[$i]['id'] == $id){
                array_splice($items, $i, 1);
                break;
            }
        }

        for($i = 0; $i < count($items); $i++){
            $total_amount += $items[$i]['cart_count'] * $items[$i]['price'];
            $total_items += $items[$i]['cart_count'];
        }

        $request->session()->put('cart.items', $items);
        $request->session()->put('cart.total_items', $total_items);
        $request->session()->put('cart.total_amount', $total_amount);
       
        // Atiduodam rezultatą
        $result = [
            "total_items"   => $total_items,
            "total_amount"  => $total_amount,
            "items"         => $items
        ];

        return $result;
    }

    public function update(Request $request)
    {        
        
        $total_items = 0;
        $total_amount = 0;
        $items = [];

        // Surandam pridedamą patiekalą DB
        $id = $request->dish_id;
        $count = $request->dish_count;

        $items = $request->session()->get('cart.items');

        for($i = 0; $i < count($items); $i++){
            if($items[$i]['id'] == $id){
                $items[$i]['cart_count'] = $count;
                break;
            }
        }

        for($i = 0; $i < count($items); $i++){
            $total_amount += $items[$i]['cart_count'] * $items[$i]['price'];
            $total_items += $items[$i]['cart_count'];
        }

        $request->session()->put('cart.items', $items);
        $request->session()->put('cart.total_items', $total_items);
        $request->session()->put('cart.total_amount', $total_amount);
       
        // Atiduodam rezultatą
        $result = [
            "total_items"   => $total_items,
            "total_amount"  => $total_amount,
            "items"         => $items
        ];

        return $result;
    }
}
