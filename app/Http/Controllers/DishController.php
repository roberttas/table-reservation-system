<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishes = \App\Dish::all();

        return view('dishes.index', compact('dishes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $selectMenus = array();
        $menus = \App\Menu::all();

        foreach ($menus as $menu) {
            $selectMenus[$menu->id] = $menu->title;
        }
        return view('dishes.create', compact('selectMenus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_dishes = \App\Dish::create($request->all());

        return redirect()->route('dishes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dishes = \App\Dish::find($id);

        return view('dishes.show', compact('dishes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dishes = \App\Dish::find($id);
        $menus = \App\Menu::all();

        $selectMenus = array();
        foreach ($menus as $menu) {
            $selectMenus[$menu->id] = $menu->title;
        }

        return view('dishes.create', compact('dishes', 'selectMenus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dishes = \App\Dish::find($id)->update($request->all());

        return redirect()->route('dishes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dishes = \App\Dish::find($id)->delete();
        return redirect()->route('dishes.index');
    }
}
