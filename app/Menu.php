<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

	protected $fillable = [
		'title',
		'description',
		'image'
	];

    public function dishes(){
    	return $this->hasMany('App\Dish'); 
    }
}
