<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         
    	DB::table('users')->insert([
            'name' => 'robertas',
            'surname' => 'ali',
            'phone_number' => '+37068888888',
            'birthdate' => '1992-05-10',
            'email' => 'robertas@example.com',
            'password' => bcrypt('abc123'),
            'address' => 'las vegas',
            'city' => 'Kaunas',
            'zip_code' => '54632',
            'country' => 'Lietuva',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
