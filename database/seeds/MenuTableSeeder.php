<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            'title' => 'Degustacinis meniu',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
