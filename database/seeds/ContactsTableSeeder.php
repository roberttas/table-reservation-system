<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('contacts')->insert([
            'name' => 'Kontaktai',
            'latitude' => 36.114647,
            'longitude' => -115.172813,
            'working_hours' => 'Darbo laikas <br />
            I-V 11:00 - 20:00<br />
            VI  10:00 - 03:00<br />
            VII 10:00 - 03:00',
            'info' => 'Papildoma informacija',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
