@extends('layouts.app')

@section('content')

<div class="row">
  @foreach ($menus as $i => $menu)
    @if($i%2 == 0)
      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">
             <h2>{{ $menu->title }}</h2> <span class="text-muted">It'll blow your mind.</span>
          </h2>
          <p class="lead">{{ $menu->description }}</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" data-src="{{ $menu->image }}" alt="Generic placeholder image">
        </div>
      </div>
      <hr class="featurette-divider">
    @else
      <div class="row featurette">
        <div class="col-md-7 col-md-push-5">
             <h2>{{ $menu->title }}</h2> <span class="text-muted">It'll blow your mind.</span>
          <p class="lead">{{ $menu->description }}</p>
        </div>
        <div class="col-md-5 col-md-pull-7">
          <img class="featurette-image img-responsive center-block" data-src="{{ $menu->image }}" alt="Generic placeholder image">
        </div>
      </div>
      <hr class="featurette-divider">
    @endif
    @foreach ($menu->dishes as $dish)
        
        
    @endforeach
  @endforeach
</div>

@endsection