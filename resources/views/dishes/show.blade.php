@extends(layouts.app)

@section('content')

	@foreach($dishes as $dish)
		<div class="col-md-4">
			<h3>
				<a href="{{ route('dishes.show', $dishd->id) }}"></a>
				{{ $dish->title }}
			</h3>
			<p>
				{{ $dish->photo }}
			</p>

		</div>

@endsection