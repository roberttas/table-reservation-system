@extends('layouts.app')

@section('content')

<a href="{{ route('dishes.create') }}">Create dish</a>

<ul>
  @foreach($dishes as $dish)
  <li>
    <h3>{{ $dish->title }}</h3>
    
    <a href="{{ route('dishes.edit', $dish->id) }}">Edit dish</a>
    <br>

    <a href="{{ route('dishes.show', $dish->id) }}">READ MORE</a>
  </li>
  @endforeach

</ul>

@endsection
