@extends("layouts.app")
@section("content")
<h3>{{ isset($dishes) ? 'Edit dish' : 'Add dish' }}</h3>
@if(isset($dishes))
    {{ Form::open(['route' => ['dishes.update', $dishes->id], 'method' => "POST"]) }}
    {{ Form::hidden('_method', 'PUT') }}
@else
    {{ Form::open(['route' => 'dishes.store', 'method' => "POST"]) }}
@endif
{{ csrf_field() }}
    {{ Form::label('title', 'Title') }}
    @if(isset($dishes))
        {{ Form::text('title', ($dishes['title']), ['class' => 'form-control']) }}
    @else
        {{ Form::text('title', '', ['class' => 'form-control']) }}
    @endif

    {{ Form::label('photo', 'Photo URL') }}
    @if(isset($dishes))
        {{ Form::text('photo', ($dishes['photo']), ['class' => 'form-control']) }}
    @else
        {{ Form::text('photo', '', ['class' => 'form-control']) }}
    @endif

    {{ Form::label('description', 'Description') }}
    @if(isset($dishes))
        {{ Form::text('description', ($dishes['description']), ['class' => 'form-control']) }}
    @else
        {{ Form::text('description', '', ['class' => 'form-control']) }}
    @endif

    {{ Form::label('netto_price', 'Netto_price') }}
    @if(isset($dishes))
        {{ Form::text('netto_price', ($dishes['netto_price']), ['class' => 'form-control']) }}
    @else
        {{ Form::text('netto_price', '', ['class' => 'form-control']) }}
    @endif

    {{ Form::label('price', 'Price') }}
    @if(isset($dishes))
        {{ Form::text('price', $dishes['price'], ['class' => 'form-control']) }}
    @else
        {{ Form::text('price', '', ['class' => 'form-control']) }}
    @endif

    {{ Form::label('quantity', 'Quantity') }}
    @if(isset($dishes))
        {{ Form::text('quantity', ($dishes['quantity']), ['class' => 'form-control']) }}
    @else
        {{ Form::text('quantity', '', ['class' => 'form-control']) }}
    @endif

    {{ Form::label('menu_id', 'menu_id') }}
    @if(isset($menu_id))
        {{ Form::select('menu_id', $selectMenus) }}

    @else
        {{ Form::select('menu_id', $selectMenus) }}
    @endif

<br>
@if(isset($dishes))
    {{ Form::submit('Edit dish', ['class' => 'btn btn-primary']) }}
@else
    {{ Form::submit('Add dish', ['class' => 'btn btn-primary']) }}
@endif
{{ Form::close() }}
@if(isset($dishes))
    {{ Form::open(['route' => ['dishes.destroy', $dishes->id], 'method' => "POST"]) }}
    {{ Form::hidden('_method', 'DELETE') }}
            {{ csrf_field() }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
    
@endif
{{ Form::close() }}
@endsection