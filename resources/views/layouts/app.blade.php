<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Reservation</title>

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Custom styles for this template -->
        <link href="{{ asset('css/jumbotron.css') }}" rel="stylesheet">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
        <style>
          @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
        </style> 

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
        var CART_UPDATE_URL = "{{ route('cart.store') }}";
        var CART_UPDATE_ONE_URL = "{{ route('cart.update') }}";
        var CART_DESTROY_ALL_URL = "{{ route('cart.destroyAll') }}";
        var CART_DESTROY_URL = "{{ route('cart.destroy') }}";
        var CSRF_TOKEN = "{{ csrf_token() }}";
        </script>
    </head>
    <body>
        <div class="container">
          <div class="header clearfix">
            <nav>
              <ul class="nav nav-pills pull-right">
                <li><a href="{{ URL::route('menu.index') }}">Admin menu</a></li>
                <li><a href="{{ URL::route('dishes.index') }}">Admin dishes menu</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Meniu
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    @foreach (\App\Menu::all() as $menu)
                    <li><a href="{{ route('menu.show', $menu->id) }}">{{ $menu->title }}</a></li>
                    @endforeach
                    <li role="separator" class="divider"></li>
                  </ul>
                </li>
                <li><a href="#">Staliukai</a></li>
                <li><a href="{{ URL::route('contacts') }}">Kontaktai</a></li>
                @if(Auth::guest())
                <li><a href="{{ URL::action('Auth\RegisterController@showRegistrationForm') }}">Register</a></li>
                @endif

                @if(Auth::check())
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                       {{ Auth::user()->name }} <span class="caret"></span>
                  </a>

                  <ul class="dropdown-menu" role="menu">
                      <li>
                          <a href="#"
                               onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                               Logout
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                               {{ csrf_field() }}
                          </form>
                      </li>
                  </ul>
                </li>
              <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-shopping-cart"></span><span class="cart-total-items">{{ Session::get('cart.total_items') }}</span></a>
                  <ul class="dropdown-menu dropdown-cart" role="menu">
                      <li>
                        <ul class="cart-items dropdownScroll">
                        @if(Session::has('cart.items'))
                          @foreach(Session::get('cart.items') as $dish)
                          <li>
                            <a data-dish-id="{{ $dish['id'] }}" class="cart-destroy" href="#">delete</a>
                            
                              {{ $dish['cart_count'] }} x {{ $dish['title'] }}  

                          </li>
                          @endforeach
                          @endif
                        </ul>
                      </li>
                      <li class="divider"></li>
                      <li><a class="text-center cart-destroy-all" href="#">Clear cart</a></li>
                      <li><a class="text-center" href="#">Total amount: <span class="cart-total-amount">{{ Session::get('cart.total_amount') }}</span> Eur</a></li>
                      <li><a class="text-center" href="{{ URL::route ('checkout') }}">Check out your all items</a></li>
                  </ul>
              </li>
                @else
                 <li><a href="{{ URL::route('login') }}">Login</a></li> 
                @endif
              </ul>

            </nav>
            <h3 class="text-muted">
              <a href="{{ URL::route('homepage') }}">Restorano rezervacija</a>
            </h3>
          </div>

            @yield('content')

            <footer class="footer">
              <p>&copy; 2016 Private Restoran, Inc.</p>
            </footer>
        </div>

        <script src="http://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
    </body>
</html>
