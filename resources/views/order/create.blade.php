@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Order data</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('order.store') }}">
                        {{ csrf_field() }}

                        @foreach ($errors->all() as $error)
                            <li>
                                <strong>{{ $error }}</strong>
                            </li>
                        @endforeach

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('table_id') ? ' has-error' : '' }}">
                            <label for="table_id" class="col-md-4 control-label">Table Nr.</label>

                            <div class="col-md-6">

                                {{ Form::select('table_id', ['1' => 'Table 1', 2 => 'Table 2']) }} 

                                @if ($errors->has('table_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('table_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('contact_phone') ? ' has-error' : '' }}">
                            <label for="contact_phone" class="col-md-4 control-label">Phone number</label>

                            <div class="col-md-6">
                                <input id="contact_phone" type="text" class="form-control" name="contact_phone" value="{{ old('contact_phone','+370') }}" required autofocus>

                                @if ($errors->has('contact_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('reserved_at') ? ' has-error' : '' }}">
                            <label for="reserved_at" class="col-md-4 control-label">Reservation: date&time</label>

                            <div class="col-md-6">
                                <input id="reserved_at" type="text" class="form-control" name="reserved_at" required value="{{ old('reserved_at') }}">
                                <p class="help-block">pvz.: 2016-05-22 20:00</p>

                                @if ($errors->has('reserved_at'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reserved_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('number_of_persons') ? ' has-error' : '' }}">
                            <label for="number_of_persons" class="col-md-4 control-label">Number of persons</label>

                            <div class="col-md-6">

                                
                                {{ Form::number('number_of_persons', 2, ['min' => 1]) }}

                                @if ($errors->has('number_of_persons'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number_of_persons') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Order
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ URL::route('checkout') }}" class="btn btn-default">
                                    Back
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection