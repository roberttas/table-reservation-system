@extends('layouts.app')

@section('content')
<div class="container">
	<table id="cart" class="table table-hover table-condensed">
		<thead>
			<tr>
				<th style="width:50%">Product</th>
				<th style="width:10%">Price</th>
				<th style="width:8%">Quantity</th>
				<th style="width:22%" class="text-center">Subtotal</th>
				<th style="width:10%"></th>
			</tr>
		</thead>
		<tbody class="checkout-items">
			@if(Session::has('cart.items'))
            @foreach(Session::get('cart.items') as $dish)
			<tr>
				<td data-th="Product">
					<div class="row">
						<div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>
						<div class="col-sm-10">
							<h4 class="nomargin">{{ $dish['title'] }}</h4>
							<p>{{ $dish['description'] }}</p>
						</div>
					</div>
				</td>
				<td data-th="Price">{{ $dish['price'] }} &#x20AC;</td>
				<td data-th="Quantity">
					<input data-dish-id="{{ $dish['id'] }}" type="number" class="form-control text-center" value="{{ $dish['cart_count'] }}">
				</td>
				<td data-th="Subtotal" class="text-center">{{ $dish['price'] * $dish['cart_count'] }} &#x20AC;</td>
				<td class="actions" data-th="">
					<button data-dish-id="{{ $dish['id'] }}" class="btn btn-info btn-sm cart-refresh"><i class="fa fa-refresh"></i></button>
					
					<button data-dish-id="{{ $dish['id'] }}" class="btn btn-danger btn-sm cart-destroy"><i class="fa fa-trash-o"></i></button>
				</td>
			</tr>
			@endforeach
            @endif
		</tbody>
		<tfoot>
			<tr class="visible-xs">
				<td class="text-center"><strong>Total 1.99</strong></td>
			</tr>
			<tr>
				<td><a href="{{ URL::route('homepage') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i>Back to Shop</a></td>
				<td colspan="2" class="hidden-xs"></td>
				<td class="hidden-xs text-center"><strong>Total price: <span class="cart-total-amount">{{ Session::get('cart.total_amount') }}</span>&#x20AC;</strong></td>
				<td><a href="{{ URL::route('order.create') }}" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
			</tr>
		</tfoot>
	</table>
</div>

@endsection
