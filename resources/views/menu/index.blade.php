@extends('layouts.app')

@section('content')

<a href="{{ route('menu.create') }}">Create meniu</a>

<ul>
  @foreach($menu as $menu)
  <li>
    <h3>{{ $menu->title }}</h3>
    
    <a href="{{ route('menu.edit', $menu->id) }}">Edit meniu</a>
    <br>

    <a href="{{ route('menu.show', $menu->id) }}">READ MORE</a>
  </li>
  @endforeach

</ul>

@endsection
