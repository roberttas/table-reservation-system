
@extends("layouts.app")
@section("content")
<h3>{{ isset($menu) ? 'Edit meniu' : 'Add meniu' }}</h3>
@if(isset($menu))
    {{ Form::open(['route' => ['menu.update', $menu->id], 'method' => "POST"]) }}
    {{ Form::hidden('_method', 'PUT') }}
@else
    {{ Form::open(['route' => 'menu.store', 'method' => "POST"]) }}
@endif
{{ csrf_field() }}
    {{ Form::label('title', 'Title') }}
    @if(isset($menu))
        {{ Form::text('title', ($menu['title']), ['class' => 'form-control']) }}
    @else
        {{ Form::text('title', '', ['class' => 'form-control']) }}
    @endif

    {{ Form::label('description', 'Description') }}
    @if(isset($menu))
        {{ Form::text('description', ($menu['description']), ['class' => 'form-control']) }}
    @else
        {{ Form::text('description', '', ['class' => 'form-control']) }}
    @endif

    {{ Form::label('image', 'Image') }}
    @if(isset($menu))
        {{ Form::text('image', ($menu['image']), ['class' => 'form-control']) }}
    @else
        {{ Form::text('image', '', ['class' => 'form-control']) }}
    @endif
<br>
@if(isset($menu))
    {{ Form::submit('Edit menu', ['class' => 'btn btn-primary']) }}
@else
    {{ Form::submit('Add menu', ['class' => 'btn btn-primary']) }}
@endif
{{ Form::close() }}
@if(isset($menu))
    {{ Form::open(['route' => ['menu.destroy', $menu->id], 'method' => "POST"]) }}
    {{ Form::hidden('_method', 'DELETE') }}
            {{ csrf_field() }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
    
@endif
{{ Form::close() }}
@endsection