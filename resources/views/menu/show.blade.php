@extends ('layouts.app')
@section ('content')

<div class="container">
    <div class="well well-sm">
        <strong>Category Title</strong>
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
            </span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
    <div id="products" class="row list-group">
        @foreach ($dishes as $index => $dish)
            <div class="item  col-xs-4 col-md-4">
                <div class="thumbnail">
                    <div class="imgClass">
                        <img class="group list-group-image img-responsive" src="{{ $dish->photo }}" alt="" />
                    </div>
                    <div class="caption">
                        <h4 class="group inner list-group-item-heading">
                            {{ $dish->title }}</h4>
                        <p class="group inner list-group-item-text">
                            {{ $dish->description }}</p>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <p class="lead">
                                    {{ $dish->price }} Euro</p>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <a data-dish-id="{{ $dish->id }}" class="btn btn-success add-to-cart" href="#">Add to cart</a>                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if(++$index % 3 == 0)
                </div><div class="row">
            @endif
        @endforeach
    </div>
</div>


@endsection