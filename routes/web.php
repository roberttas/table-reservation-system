<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
	$menus = \App\Menu::all();
    return view('welcome', compact('menus'));
})->name('homepage');


Route::resource("/menu", "MenuController");
Route::resource("/dishes", "DishController");


Route::get('/contacts', function() {
  $contacts = \App\Contacts::first();

  return view('contacts.index', compact('contacts'));
})->name('contacts');


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('/cart', 'CartController@store')->name('cart.store');

Route::post('/cartDestroyAll', 'CartController@destroyAll')->name('cart.destroyAll');

Route::post('/cartDestroy', 'CartController@destroy')->name('cart.destroy');

Route::get('checkout', function () {
	return view('checkout'); 
})->name('checkout');

Route::post('/cartUpdate', 'CartController@update')->name('cart.update');

Route::resource('/order', 'OrderController');