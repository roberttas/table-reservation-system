function refreshCart() {
		var id = $(this).data('dish-id');
		var count = $('input[data-dish-id="'+id+'"]').val();

		$.ajax({
			method: "POST",
			url: CART_UPDATE_ONE_URL,
			data: {
				dish_id: id,
				dish_count: count,
				_token: CSRF_TOKEN
			},
			success: function(response) {
				// Užklausa sėkminga
				
				$('.cart-total-items').html(response.total_items);
				$('.cart-total-amount').html(response.total_amount);

				$('.checkout-items').html(" ");
				for(var i = 0; i < response.items.length; i++){

					var item = '<tr>' +
						'<td data-th="Product">' +
							'<div class="row">' +
								'<div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>' +
								'<div class="col-sm-10">' +
									'<h4 class="nomargin">' + response.items[i].title  + '</h4>' +
									'<p>' + response.items[i].description + '</p>' +
								'</div>' + 
							'</div>' +
						'</td>' +
						'<td data-th="Price">' + response.items[i].price + '&#x20AC;</td>' +
						'<td data-th="Quantity">' +
							'<input data-dish-id="' + response.items[i].id + '" type="number" class="form-control text-center" value="' + response.items[i].cart_count + '">' +
						'</td>' +
						'<td data-th="Subtotal" class="text-center">' + response.items[i].price * response.items[i].cart_count + '&#x20AC;</td>' +
						'<td class="actions" data-th="">' +
							'<button data-dish-id="' + response.items[i].id + '" class="btn btn-info btn-sm cart-refresh"><i class="fa fa-refresh"></i></button>' +
							
							'<button data-dish-id="' + response.items[i].id + '" class="btn btn-danger btn-sm cart-destroy"><i class="fa fa-trash-o"></i></button>' +

						'</td>' +
					'</tr>';

					$('.checkout-items').append(item);

				}
					$('.cart-refresh').click(refreshCart);
				$('.cart-destroy').click(removeCartItem);
			},
			fail: function(error) {
				// Užklausa nepavyko
				console.log('Fail');
				console.log(error);
			}
		});
	}

function removeCartItem(){
		
	var id = $(this).data('dish-id');

		$.ajax({
		method: "POST",
		url: CART_DESTROY_URL,
		data: {
			dish_id: id,
			_token: CSRF_TOKEN
		},
		success: function(response) {
			// Užklausa sėkminga
			
			$('.cart-total-items').html(response.total_items);
			$('.cart-total-amount').html(response.total_amount);

			$('.cart-items').html(" ");
			$('.checkout-items').html(" ");
			for(var i = 0; i < response.items.length; i++){
				$('.cart-items').append('<li><a data-dish-id="' + response.items[i].id + '" class="cart-destroy" href="#">delete</a> ' + response.items[i].cart_count + ' x ' + response.items[i].title + '</li>')
				
				var item = '<tr>' +
				'<td data-th="Product">' +
					'<div class="row">' +
						'<div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>' +
						'<div class="col-sm-10">' +
							'<h4 class="nomargin">' + response.items[i].title  + '</h4>' +
							'<p>' + response.items[i].description + '</p>' +
						'</div>' + 
					'</div>' +
				'</td>' +
				'<td data-th="Price">' + response.items[i].price + '&#x20AC;</td>' +
				'<td data-th="Quantity">' +
					'<input type="number" class="form-control text-center" value="' + response.items[i].cart_count + '">' +
				'</td>' +
				'<td data-th="Subtotal" class="text-center">' + response.items[i].price * response.items[i].cart_count + '&#x20AC;</td>' +
				'<td class="actions" data-th="">' +
					'<button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>' +
					
					'<button data-dish-id="' + response.items[i].id + '" class="btn btn-danger btn-sm cart-destroy"><i class="fa fa-trash-o"></i></button>' +

				'</td>' +
			'</tr>';

				$('.checkout-items').append(item);
			}
			$('.cart-destroy').click(removeCartItem);
		},
		fail: function(error) {
			// Užklausa nepavyko
			console.log('Fail');
			console.log(error);
		}
	});
};



$(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});

    $('.cart-destroy').click(removeCartItem);

    $('.cart-destroy-all').click(function() {
		$.ajax({
			method: "POST",
			url: CART_DESTROY_ALL_URL,
			data: {
				_token: CSRF_TOKEN
			},
			success: function(response) {
				// Užklausa sėkminga
				$('.cart-total-items').html(0);
				$('.cart-total-amount').html(0);

				$('.cart-items').html(" ");
			},
			fail: function(error) {
				// Užklausa nepavyko
				console.log('Fail');
				console.log(error);
			}
		});
	});

    $('.add-to-cart').click(function() {
		var id = $(this).data('dish-id');

		$.ajax({
			method: "POST",
			url: CART_UPDATE_URL,
			data: {
				dish_id: id,
				_token: CSRF_TOKEN
			},
			success: function(response) {
				// Užklausa sėkminga
				
				$('.cart-total-items').html(response.total_items);
				$('.cart-total-amount').html(response.total_amount);

				$('.cart-items').html(" ");
				for(var i = 0; i < response.items.length; i++){
					$('.cart-items').append('<li><a data-dish-id="' + response.items[i].id + '" class="cart-destroy" href="#">delete</a><a href="#">' + response.items[i].cart_count + ' x ' + response.items[i].title + '</a></li>')
				}
				$('.cart-destroy').click(removeCartItem);
			},
			fail: function(error) {
				// Užklausa nepavyko
				console.log('Fail');
				console.log(error);
			}

		});
	});

	$('.cart-refresh').click(refreshCart);
});
